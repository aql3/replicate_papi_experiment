write_up/MaleskyLe_PAPI_experiment.pdf: write_up/library.bib write_up/MaleskyLe_PAPI_experiment.tex table/pretreatment_balance.tex figure/provinces_treatment.pdf figure/outcome_non_normal_error.pdf table/teachingtothetest.tex table/equivalence_test_measurement_bias.tex write_up
	cd write_up;pdflatex MaleskyLe_PAPI_experiment
	cd write_up;bibtex MaleskyLe_PAPI_experiment
	cd write_up;pdflatex MaleskyLe_PAPI_experiment
	cd write_up;pdflatex MaleskyLe_PAPI_experiment

# Create sub-folders
table:
	mkdir table
figure:
	mkdir figure
result:
	mkdir result
write_up:
	mkdir write_up

# Pre-treatment balance
table/pretreatment_balance.tex: R/13_pretreat_balance_test.R data/ts_20042013.dta output_data/treatment_status.RData table
	cd R;Rscript 13_pretreat_balance_test.R

# Vietnam map
figure/provinces_treatment.pdf: R/32_vis_vietnam.R output_data/treatment_status.RData figure
	cd R;Rscript 32_vis_vietnam.R

# Visualize non-normal error
figure/outcome_non_normal_error.pdf: R/33_vis_nonnormal.R figure
	cd R;Rscript 33_vis_nonnormal.R

# Randomization test
randomization_test_results: result/randomization_test_governance.RData result/randomization_test_dimension3.RData result/randomization_test_dimension5.RData result/randomization_test_sub63.RData result/randomization_test_acrossyears.RData

result/randomization_test_governance%RData result/randomization_test_dimension3%RData result/randomization_test_dimension5%RData result/randomization_test_sub63%RData result/randomization_test_acrossyears%RData: R/_function_randomization_test.R R/21_randomization_tests.R result
	cd R;Rscript 21_randomization_tests.R

# Randomization test
# randomization_test_results: result/randomization_test_governance.RData result/randomization_test_dimension3.RData result/randomization_test_dimension5.RData result/randomization_test_sub63.RData result/randomization_test_acrossyears.RData

# result/randomization_test_governance.RData: R/_function_randomization_test.R R/21_randomization_tests.R result
# 	cd R;Rscript 21_randomization_tests.R

# result/randomization_test_dimension3.RData: R/_function_randomization_test.R R/21_randomization_tests.R result
# 	cd R;Rscript 21_randomization_tests.R

# result/randomization_test_dimension5.RData: R/_function_randomization_test.R R/21_randomization_tests.R result
# 	cd R;Rscript 21_randomization_tests.R

# result/randomization_test_sub63.RData: R/_function_randomization_test.R R/21_randomization_tests.R result
# 	cd R;Rscript 21_randomization_tests.R

# result/randomization_test_acrossyears.RData: R/_function_randomization_test.R R/21_randomization_tests.R result
# 	cd R;Rscript 21_randomization_tests.R

# Visualizing randomization test result

vis_randomization_test: figure/randomization_test_dimension5.pdf figure/randomization_test_subdimension5.pdf figure/randomization_test_sub_infrastructure.pdf figure/randomization_test_dimension3.pdf figure/randomization_test_subdimension3.pdf figure/randomization_test_acrossyears.pdf
figure/randomization_test_dimension5%pdf figure/randomization_test_subdimension5%pdf figure/randomization_test_sub_infrastructure%pdf figure/randomization_test_dimension3%pdf figure/randomization_test_subdimension3%pdf figure/randomization_test_acrossyears%pdf: R/_function_papi_boxplot.R R/31_vis_randomization.R figure
	cd R;Rscript 31_vis_randomization.R
# VIS_RANDOMIZATION_TEST_FILES = figure/randomization_test_dimension5.pdf figure/randomization_test_subdimension5.pdf figure/randomization_test_dimension6.pdf figure/randomization_test_subdimension6.pdf figure/randomization_test_infrastructure.pdf figure/randomization_test_sub_infrastructure.pdf figure/randomization_test_dimension3.pdf figure/randomization_test_subdimension3.pdf figure/randomization_test_acrossyears.pdf
# vis_randomization_test: $(VIS_RANDOMIZATION_TEST_FILES)

# figure/randomization_test_dimension5.pdf: R/_function_papi_boxplot.R R/31_vis_randomization.R figure
# 	cd R;Rscript 31_vis_randomization.R
# figure/randomization_test_subdimension5.pdf: R/_function_papi_boxplot.R R/31_vis_randomization.R figure
# 	cd R;Rscript 31_vis_randomization.R
# figure/randomization_test_dimension6.pdf: R/_function_papi_boxplot.R R/31_vis_randomization.R figure
# 	cd R;Rscript 31_vis_randomization.R
# figure/randomization_test_subdimension6.pdf: R/_function_papi_boxplot.R R/31_vis_randomization.R figure
# 	cd R;Rscript 31_vis_randomization.R
# figure/randomization_test_infrastructure.pdf: R/_function_papi_boxplot.R R/31_vis_randomization.R figure
# 	cd R;Rscript 31_vis_randomization.R
# figure/randomization_test_sub_infrastructure.pdf: R/_function_papi_boxplot.R R/31_vis_randomization.R figure
# 	cd R;Rscript 31_vis_randomization.R
# figure/randomization_test_dimension3.pdf: R/_function_papi_boxplot.R R/31_vis_randomization.R figure
# 	cd R;Rscript 31_vis_randomization.R
# figure/randomization_test_subdimension3.pdf: R/_function_papi_boxplot.R R/31_vis_randomization.R figure
# 	cd R;Rscript 31_vis_randomization.R
# figure/randomization_test_acrossyears.pdf: R/_function_papi_boxplot.R R/31_vis_randomization.R figure
# 	cd R;Rscript 31_vis_randomization.R

# Equivalence test
table/equivalence_test_measurement_bias.tex: R/23_equivalence_test.R output_data/PCI2011.dta
	cd R;Rscript 23_equivalence_test.R
