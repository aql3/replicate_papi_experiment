/*
* Purpose:
	- Aggregate indicators up to provincial level to do randomization test
* Program used:
* Input files:
	output_data/PAPI_from11_to14.dta
* Output files:
*/
capture log close
clear
set more off
set matsize 5000

* Change working directory to your folder
capture cd "C:\Users\ejm5\Dropbox\papi_experiment"
capture cd "C:\Users\aql3\Dropbox\Malesky\papi_experiment"
capture cd "/home/anh/Dropbox/Malesky/papi_experiment"

/* Load data and clean up */
use "output_data/PAPI_from11_to14.dta", clear

* Set survey weight for PAPI
svyset PSW_PSU [pweight=PSW_psweight], fpc(PSW_FPC1) strata(PSW_STRATA) singleunit(certainty) /// 
	|| PSW_SSU, fpc(PSW_FPC2) strata(PSW_STRATA2) ///
	|| villageid, fpc(PSW_FPC3) strata(PSW_STRATA3) ///
	|| _n, poststrata(poststrata) postweight(postweight)

/* Get provincial means for PAPI dimension scores */
global papi_governance dimension?
foreach outcome of varlist $papi_governance {
	set matsize 11000
	svy, subpop(if year == 2011): mean `outcome', over(tinh)

	putexcel A1 = matrix(e(b)) using "result/export_`outcome'", sheet("est") replace
	putexcel A1 = matrix(e(V)) using "result/export_`outcome'", sheet("variance") modify
	putexcel A1 = (e(over_labels)) using "result/export_`outcome'", sheet("label") modify
}

/* Get provincial means for dimension 5 across years */
set more off
* 2014 missing 2 provinces due to data irregularities
foreach year in 2011 2012 2013 {
	set matsize 11000
	svy, subpop(if year == `year'): mean dimension5, over(tinh)

	putexcel A1 = matrix(e(b)) using "result/export_dim5_`year'", sheet("est") replace
	putexcel A1 = matrix(e(V)) using "result/export_dim5_`year'", sheet("variance") modify
	putexcel A1 = (e(over_labels)) using "result/export_dim5_`year'", sheet("label") modify
}

/* Get provincial means for Dimension 3-Vertical Accountability indicators */
set more off
global papi_dimension3 sub3_1 sub3_2 sub3_3
* Interaction with Local Authorities, People's Inspection Boards, Community Investment Boards
foreach outcome of varlist $papi_dimension3 {
	set matsize 11000
	svy, subpop(if year == 2011): mean `outcome', over(tinh)

	putexcel A1 = matrix(e(b)) using "result/export_`outcome'", sheet("est") replace
	putexcel A1 = matrix(e(V)) using "result/export_`outcome'", sheet("variance") modify
	putexcel A1 = (e(over_labels)) using "result/export_`outcome'", sheet("label") modify
}

/* Get provincial means for Dimension 5-Admin Procedures indicators */
set more off
global papi_dimension5 sub5_1 sub5_2 sub5_3 sub5_4
* Certification Procedures, Construction Permit, Land, Other Procedures
foreach outcome of varlist $papi_dimension5 {
	set matsize 11000
	svy, subpop(if year == 2011): mean `outcome', over(tinh)

	putexcel A1 = matrix(e(b)) using "result/export_`outcome'", sheet("est") replace
	putexcel A1 = matrix(e(V)) using "result/export_`outcome'", sheet("variance") modify
	putexcel A1 = (e(over_labels)) using "result/export_`outcome'", sheet("label") modify
}

/* Get provincial means for infrastructure indicators */
set more off
global papi_infrastructure d607 d608 d609 tapwater
foreach outcome of varlist $papi_infrastructure {
		set matsize 11000
	svy, subpop(if year == 2011): mean `outcome', over(tinh)
	
	putexcel A1 = matrix(e(b)) using "result/export_`outcome'", sheet("est") replace
	putexcel A1 = matrix(e(V)) using "result/export_`outcome'", sheet("variance") modify
	putexcel A1 = (e(over_labels)) using "result/export_`outcome'", sheet("label") modify
}
