/*
* Created by: Anh Le
* Last modified: Anh Le
* Purpose:
 	We know that there is treatment effect in PAPI's dim 5 admin proc
	Now we want to test if the same effect shows up in PCI's admin proc
* We want to see if provinces "teach to the test". 

* Input files:
	output_data/PCI2011.dta
*/
capture log close
clear
set more off

* Change working directory to your folder
capture cd "D:/Dropbox/Malesky/papi_experiment"
capture cd "C:\Users\ejm5\Dropbox\papi_experiment"
capture cd "C:\Users\aql3\Dropbox\Malesky\papi_experiment"
capture cd "/home/anh/Dropbox/Malesky/papi_experiment"

use "output_data/PCI2011.dta", clear

eststo clear
foreach dvar of varlist d9_1 d9_2 d9_3 d9_4 {
	eststo: reg `dvar' i.papi2010tinh, cluster(tinh)
}
esttab using "table/teachingtothetest.tex", ///
	mtitles("Friendlier Officials" "Fewer Visits" ///
			"Reduced Paperwork" "Reduced Fee") ///
			nobaselevels se r2 ///
			star(+ 0.10 * 0.05 ** 0.01) ///
			coeflabels(1.papi2010tinh "Treatment" _cons "Intercept") replace
